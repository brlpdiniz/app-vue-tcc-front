<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BacklogBoard extends Model
{
    protected $fillable = [
    'user_id',
    'titulo', 
    'descricao',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
