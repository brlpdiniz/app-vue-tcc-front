<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    use HasFactory;

    protected $primaryKey = 'card_id';

    protected $fillable = [
        'card_title',
        'card_description',
        'column_id',
        'start_date',
        'end_date',   
    ];
    
    public function column()
    {
        return $this->belongsTo(Column::class, 'column_id', 'column_id');
    }
}
