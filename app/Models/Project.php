<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $primaryKey = 'project_id';

    protected $fillable = [
        'user_id',
        'project_name',
        'start_date',
        'end_date',
    ];

    protected $casts = [
        'project_id' => 'string',
        'back_log_id' => 'string',
    ];

    protected $hidden = ['created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function columns()
    {
        return $this->hasMany(Column::class, 'project_id');
    }
}
