<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Column extends Model
{
    use HasFactory;

    protected $primaryKey = 'column_id';

    protected $fillable = [
        'column_title',
        'column_description',
        'project_id',
    ];

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'project_id');
    }

    public function cards()
    {
        return $this->hasMany(Card::class, 'column_id', 'column_id');
    }
}
