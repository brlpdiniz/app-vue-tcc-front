<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\User;
use Exception;

class ProjectController extends Controller
{
    private $return;

    public function __construct()
    {
        $this->return = [];
    }

    public function getProjectDetails(Request $request)
    {
        try {
            // Encontre o projeto pelo ID
            $project = Project::with('columns.cards')->find($request->project_id);

            if (!$project) {
                return response(['success' => false, 'message' => 'Projeto não encontrado'], 404);
            }

            // Retorne os detalhes do projeto com colunas e cards
            return response(['success' => true, 'data' => $project], 200);
        } catch (Exception $e) {
            return response(['success' => false, 'message' => 'Erro ao obter detalhes do projeto: ' . $e->getMessage()], 500);
        }
    }

    public function index(Request $request)
    {
        try {
            $projectQuery = Project::query();
            if (!empty($request->user_id)) {
                $projectQuery->where('user_id', $request->user_id);
            }
            $cardList = $projectQuery->get();
            return response(['success' => true, 'data' => $cardList], 200);
        } catch (Exception $e) {
            return response(['success' => false, 'message' => 'erro ao buscar projetos: '], 200);
        }
    }

    public function porcentagem(Request $request)
    {
        try {
            $project = Project::with('columns.cards')->find($request->project_id);
    
            if (!$project) {
                return response(['success' => false, 'message' => 'Projeto não encontrado'], 404);
            }

            $allCards = $project->columns->flatMap(function ($column) {
                return $column->cards;
            });
    
            $finalizedCards = $project->columns->filter(function ($column) {
                return $column->column_title === 'Done';
            })->flatMap(function ($column) {
                return $column->cards;
            });
    
            $totalCardsCount = count($allCards);
            $finalizedCardsCount = count($finalizedCards);
    
            return response([
                'success' => true,
                'total_cards_count' => $totalCardsCount,
                'finalized_cards_count' => $finalizedCardsCount,
            ], 200);
        } catch (Exception $e) {
            return response(['success' => false, 'message' => 'Erro ao buscar projetos'], 500);
        }
    }
    

    public function store(Request $request)
    {
        try {
            // Certifique-se de que o usuário existe antes de associar o projeto
            $user = User::find($request->user_id);

            if (!$user) {
                return response(['success' => false, 'message' => 'Usuário não encontrado'], 404);
            }

            // Crie o projeto associado ao usuário
            $project = $user->projects()->create([
                'project_name' => $request->project_name,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
            ]);

            // Agora, chame o método de criação de colunas no ColumnController
            app(ColumnController::class)->createDefaultColumns($project->project_id);

            $this->return = ['success' => true, 'data' => $project];
            return response($this->return, 200);
        } catch (\Exception $e) {
            return response(['success' => false, 'message' => 'Erro ao criar projeto: ' . $e->getMessage()], 400);
        }
    }

    public function update(Request $request)
    {
        try {
            $project = Project::find($request->project_id);
    
            if ($project) {
                $project->project_name = $request->project_name ?? $project->project_name;
                $project->start_date = $request->start_date ?? $project->start_date;
                $project->end_date = $request->end_date ?? $project->end_date;
    
                $projectUpdated = $project->save();
    
                if ($projectUpdated) {
                    $this->return = ['success' => true, 'data' => 'Projeto atualizado com sucesso'];
                } else {
                    $this->return = ['success' => false, 'message' => 'Erro ao atualizar projeto'];
                }
            } else {
                return response("Projeto não encontrado", 404);
            }
    
            return response($this->return, 200);
        } catch (Exception $e) {
            return response(['success' => false, 'message' => 'Erro ao atualizar projeto: ' . $e->getMessage()], 400);
        }
    }
    

    public function destroy(Request $request)
    {
        try {
            $project = Project::find($request->project_id);
    
            if ($project) {
                $project->delete();
                $this->return = ['success' => true, 'message' => 'Projeto deletado com sucesso'];
            } else {
                $this->return = ['success' => false, 'message' => 'Projeto não encontrado'];
            }
    
            return response($this->return, 200);
        } catch (Exception $e) {
            return response($this->handlerException($e), 200);
        }
    }
}
