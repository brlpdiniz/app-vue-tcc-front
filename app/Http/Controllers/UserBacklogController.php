<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BacklogBoard;

class UserBacklogController extends Controller
{

    public function __construct()
    {
        $this->return = [];
    }

    public function index(Request $request)
    {
        try {
            $backLogQuery = BacklogBoard::query();
            if (!empty($request->user_id)) {
                $backLogQuery->where('user_id', $request->user_id);
            }
            $cardList = $backLogQuery->get();
            return response(['success' => true, 'data' => $cardList], 200);
        } catch (Exception $e) {
            return response(['success' => false, 'message' => 'erro ao buscar cards do Product Backlog: '], 200);
        }
    }

    public function store(Request $request)
    {
        try {
            $backLogStored = BacklogBoard::create([
                'user_id' => $request->user_id,
                'titulo' => $request->titulo,
                'descricao' => $request->descricao,
            ]);
            if ($backLogStored) {
                $this->return = ['success' => true, 'data' => $backLogStored];
            } else {
                $this->return = ['success' => false, 'message' => 'erro ao criar Card'];
            }
            return response($this->return, 200);
        } catch (Exception $e) {
            return response(['success' => false, 'message' => 'erro ao criar Card: '], 400);
        }
    }
}
