<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Card;
use App\Models\Column;
use Exception;

class CardController extends Controller
{
    private $return;

    public function __construct()
    {
        $this->return = [];
    }

    public function index(Request $request)
    {
        try {
            $cardQuery = Card::query();
            if (!empty($request->column_id)) {
                $cardQuery->where('column_id', $request->column_id);
            }
            $columnList = $cardQuery->get();
            return response(['success' => true, 'data' => $columnList], 200);
        } catch (Exception $e) {
            return response(['success' => false, 'message' => 'erro ao buscar cards: '], 200);
        }
    }

    public function store(Request $request)
    {
        try {
            // Verifica se a coluna de Product Backlog existe
            $backlogColumn = Column::where('column_title', 'Product Backlog')->where('column_id', $request->column_id)->first();
    
            if (!$backlogColumn) {
                return response(['success' => false, 'message' => 'A coluna de Product Backlog não foi encontrada'], 404);
            }

            $cardCreated = Card::create([
                'column_id' => $request->column_id,
                'card_title' => $request->titulo,
                'card_description' => $request->descricao,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
            ]);
    
            if ($cardCreated) {
                return response(['success' => true, 'message' => 'Card criado com sucesso'], 200);
            } else {
                return response(['success' => false, 'message' => 'Erro ao criar card'], 200);
            }
        } catch (Exception $e) {
            return response(['success' => false, 'message' => 'Erro ao criar card: ' . $e->getMessage()], 200);
        }
    }

    public function update(Request $request)
    {
        try {
            $card = Card::find($request->card_id);
            if ($card) {
                $card->card_title = isset($request->titulo) ? $request->titulo : $card->card_title;
                $card->card_description = isset($request->descricao) ? $request->descricao : $card->card_description;

                $cardUpdated = $card->save();

                if ($cardUpdated === true) {
                    $this->return = ['success' => true, 'data' => 'Card atualizado com sucesso',];
                } else {
                    $this->return = ['success' => false, 'message' => 'erro ao atualizar card',];
                }
            } else {
                $this->return = ['success' => false, 'message' => "Card não encontrado"];
            }
            return response($this->return, 200);
        } catch (Exception $e) {
            return response($this->handlerException($e), 200);
        }
    }

    public function destroy(Request $request)
    {
        try {
            $card = Card::find($request->card_id);
            if ($card) {
                $card->delete();
                $this->return = ['success' => true, 'message' => 'Card deletado com sucesso'];
            } else {
                $this->return = ['success' => false, 'message' => 'Card não encontrado'];
            }
            return response($this->return, 200);
        } catch (Exception $e) {
            return response(['success' => false, 'message' => 'Erro inesperado, favor entre em contato com o suporte'], 400);
        }
    }

    public function moveCard(Request $request)
    {
        try {
            // Verifica se a coluna de destino existe
            $destinationColumn = Column::find($request->destination_column_id);

            if (!$destinationColumn) {
                return response(['success' => false, 'message' => 'A coluna de destino não foi encontrada'], 404);
            }
    
            // Verifica se o card existe
            $card = Card::find($request->card_id);
            if (!$card) {
                return response(['success' => false, 'message' => 'O card não foi encontrado'], 404);
            }
    
            // Move o card para a coluna de destino
            $card->column_id = $destinationColumn->column_id; // Usar column_id da coluna de destino
            $card->save();
    
            return response(['success' => true, 'message' => 'Card movido com sucesso para a coluna de destino'], 200);
        } catch (Exception $e) {
            return response(['success' => false, 'message' => 'Erro ao mover card: ' . $e->getMessage()], 500);
        }
    }
}
