<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Column;
use Exception;

class ColumnController extends Controller
{
    private $return;

    public function __construct()
    {
        $this->return = [];
    }

    public function index(Request $request)
    {
        try {
            $columnQuery = Column::query();
            if (!empty($request->project_id)) {
                $columnQuery->where('project_id', $request->project_id);
            }
            $columnList = $columnQuery->get();
            return response(['success' => true, 'data' => $columnList], 200);
        } catch (Exception $e) {
            return response(['success' => false, 'message' => 'erro ao buscar colunas: '], 200);
        }
    }

    public function store(Request $request)
    {
        try {
            $columnCreated = Column::create([
                'project_id' => $request->project_id,
                'column_title' => $request->titulo,
                'column_description' => $request->descricao,
                'card_id' => $request->card_id,
            ]);

            if ($columnCreated) {
                return response(['success' => true, 'message' => 'Coluna criada com sucesso'], 200);
            } else {
                return response(['success' => false, 'message' => 'Erro ao criar coluna'], 200);
            }
        } catch (Exception $e) {
            return response(['success' => false, 'message' => 'Erro ao criar coluna: ' . $e->getMessage()], 200);
        }
    }

    // Método público para criar as colunas padrão
    public function createDefaultColumns($projectId)
    {
        $columns = [
            ['column_title' => 'Product Backlog', 'order' => 1],
            ['column_title' => 'Backlog Sprint', 'order' => 2],
            ['column_title' => 'Sprint', 'order' => 3],
            ['column_title' => 'Test Phase', 'order' => 4],
            ['column_title' => 'Done', 'order' => 5],
        ];

        foreach ($columns as $column) {
            Column::create([
                'project_id' => $projectId,
                'column_title' => $column['column_title'],
                'order' => $column['order'],
            ]);
        }
    }

    public function update(Request $request)
    {
        try {
            $column = Column::find($request->column_id);
    
            if ($column) {
                //coluna Product Backlog ou Done, apenas a atualização da descrição
                if ($column->column_title === 'Product Backlog' || $column->column_title === 'Done') {
                    $column->column_description = isset($request->descricao) ? $request->descricao : $column->column_description;
                } else {
                    $column->column_title = isset($request->titulo) ? $request->titulo : $column->column_title;
                    $column->column_description = isset($request->descricao) ? $request->descricao : $column->column_description;
                }
                $columnUpdated = $column->save();
                if ($columnUpdated) {
                    $this->return = ['success' => true, 'data' => 'Coluna atualizada com sucesso'];
                } else {
                    $this->return = ['success' => false, 'message' => 'Erro ao atualizar coluna'];
                }
                return response($this->return, 200);
            } else {
                return response("Coluna não encontrada", 404);
            }
        } catch (Exception $e) {
            return response($this->handlerException($e), 200);
        }
    }

    public function destroy(Request $request)
    {
        try {
            $column = Column::find($request->column_id);
    
            if ($column) {
                if ($column->column_title === 'Product Backlog' || $column->column_title === 'Done') {
                    $this->return = ['success' => false, 'message' => 'Não é permitido excluir a coluna ' . $column->column_title ];
                } else {
                    $column->delete();
                    $this->return = ['success' => true, 'message' => 'Coluna deletada com sucesso'];
                }
            } else {
                $this->return = ['success' => false, 'message' => 'Coluna não encontrada'];
            }
    
            return response($this->return, 200);
        } catch (Exception $e) {
            return response($this->handlerException($e), 200);
        }
    }
}