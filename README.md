# Manual de Funcionamento - Front-end

| GRR | NOME |
| ------ | ------ |
| 2017208552 | Bruno Leandro Diniz |
| 2021161408 | João Vitor Ribeiro de Moraes |
| 2020245236 | Levi Passos do Pinho |
| 2020132435 | José Kozechen Farias |

- Clonar o repositorio GIT onde está alocado o Front-end:
```
git clone https://gitlab.com/brlpdiniz/app-vue-tcc-front.git
```
- Instalar framework do Nuxt:
```
npm install -g create-nuxt-app
```
- Instalar pacotes do projeto:
```
npm install
```
Iniciar servidor Front-end:
```
npm run dev
```