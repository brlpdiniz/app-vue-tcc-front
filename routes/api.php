<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserBacklogController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\ColumnController;
use App\Http\Controllers\CardController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
// LOGIN
Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);
// PROJETO
Route::post('/list-project', [ProjectController::class, 'index']);
Route::post('/list-project/porcentagem', [ProjectController::class, 'porcentagem']);
Route::post('/create-project', [ProjectController::class, 'store']);
Route::post('/update-project', [ProjectController::class, 'update']);
Route::post('/delete-project', [ProjectController::class, 'destroy']);

Route::post('/details-project', [ProjectController::class, 'getProjectDetails']);
// COLUNA
Route::post('/create-columns', [ColumnController::class, 'store']);
Route::post('/list-columns', [ColumnController::class, 'index']);
Route::post('/update-columns', [ColumnController::class, 'update']);
Route::post('/delete-columns', [ColumnController::class, 'destroy']);
// CARD
Route::post('/create-cards', [CardController::class, 'store']);
Route::post('/list-cards', [CardController::class, 'index']);
Route::post('/update-cards', [CardController::class, 'update']);
Route::post('/delete-cards', [CardController::class, 'destroy']);
Route::post('/move-cards', [CardController::class, 'moveCard']);